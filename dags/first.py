from io import BytesIO

import pandas as pd
import pendulum

from airflow.decorators import dag, task
from airflow.providers.google.cloud.hooks.bigquery import BigQueryHook
from airflow.providers.google.cloud.hooks.gcs import GCSHook
from airflow.providers.google.cloud.operators.bigquery import BigQueryCreateEmptyDatasetOperator
from airflow.providers.google.cloud.transfers.gcs_to_bigquery import GCSToBigQueryOperator
from airflow.providers.google.cloud.transfers.postgres_to_gcs import PostgresToGCSOperator


@task
def compute_price_and_extract():
    bq_hook = BigQueryHook(gcp_conn_id="gcp", location="EU")
    gcs_hook = GCSHook(gcp_conn_id="gcp")

    data = bq_hook.get_records("SELECT SUM(price) FROM raw.fruits")
    df = pd.DataFrame(data)

    output = BytesIO()
    df.to_excel(output)
    output.seek(0)

    gcs_hook.upload("postgres-raw-landing", "prices.xlsx", data=output.read())


@dag(
    schedule="0 1 * * *",
    start_date=pendulum.datetime(2022, 1, 1, tz="UTC"),
    catchup=False,
)
def my_first_pipeline():
    """
    ### Prepare and load data
    This is the DAG that loads all the raw data
    """
    BUCKET = "postgres-raw-landing"
    FILENAME = "fruits.csv"

    upload_data = PostgresToGCSOperator(
        task_id="fruits_pg_to_gcs",
        postgres_conn_id="postgres",
        gcp_conn_id="gcp",
        sql="SELECT * FROM fruits",
        bucket=BUCKET,
        filename=FILENAME,
        gzip=False,
        export_format="csv",
    )

    create_dataset = BigQueryCreateEmptyDatasetOperator(
        task_id="create_dataset",
        dataset_id="raw",
        location="EU",
        gcp_conn_id="gcp",
    )

    upload_erf_to_bq = GCSToBigQueryOperator(
        task_id="upload_raw_fruits",
        gcp_conn_id="gcp",
        bucket=BUCKET,
        source_objects=[FILENAME],
        destination_project_dataset_table="raw.fruits",
        write_disposition="WRITE_TRUNCATE",
        location="EU",
        source_format="CSV",
        autodetect=True,
    )

    upload_data >> create_dataset >> upload_erf_to_bq >> compute_price_and_extract()


my_first_pipeline()
